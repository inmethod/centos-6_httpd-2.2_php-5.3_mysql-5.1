#!/bin/bash 
/etc/rc.d/init.d/mysqld start
/usr/bin/mysqladmin -u root password 'new-password'
mysql --batch --silent  -pnew-password -uroot -e  "DELETE FROM mysql.user ;CREATE USER  'root'@'%' IDENTIFIED BY '${MYSQL_ROOT_PASSWORD}' ;GRANT ALL ON *.* TO 'root'@'%' WITH GRANT OPTION ;DROP DATABASE IF EXISTS test ;flush privileges"
if [ "${register_globals}" = "On" ]; then 
  sed -i -- 's/register_globals = Off/register_globals = On/g' /etc/php.ini 
fi
if [ "${short_open_tag}" = "On" ]; then
 sed -i -- 's/short_open_tag = Off/short_open_tag = On/g' /etc/php.ini
fi
if [ -n "${upload_max_filesize}" ]; then
 sed -i -- 's/upload_max_filesize = 2M/upload_max_filesize = '"${upload_max_filesize}"'/g' /etc/php.ini
fi
chown -R apache:apache /var/www/html
exec httpd -DFOREGROUND

